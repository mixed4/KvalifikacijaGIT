﻿namespace Kvalifikacija.Models
{
    //Breakdown model
    public class Kvar
    {
        public int IdKvara { get; set; }

        public string Naziv { get; set; }

        public string ImeStroja { get; set; }

        public int Prioritet { get; set; }

        public DateTime VrijemePocetka { get; set; }

        public DateTime VrijemeZavrsetka { get; set; }

        public string Opis { get; set; }

        public bool Status { get; set; }
    }
}
