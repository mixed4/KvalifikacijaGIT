﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kvalifikacija.Models
{
    //Machine model
    public class Stroj
    {
        public int IdStroja { get; set; }

        public string ImeStroja { get; set; }

        public string ProsjekTrajanja { get; set; }

        public List<Kvar> KvarList { get; set; }= new List<Kvar>();
    }
}
