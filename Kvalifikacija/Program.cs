using Kvalifikacija.Context;
using Kvalifikacija.Repository;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Data;
using dapper.postgre.api.Context;
using Kvalifikacija.Services;

var builder = WebApplication.CreateBuilder(args);


builder.Services.Configure<DbSettings>(builder.Configuration.GetSection("DbSettings"));

builder.Services.AddControllers();
builder.Services.AddSingleton<DapperContext>(); 
builder.Services.AddScoped<IStrojeviRepository, StrojeviRepository>();
builder.Services.AddScoped<IStrojService, StrojService>();
builder.Services.AddScoped<IKvarRepository, KvarRepository>();
builder.Services.AddScoped<IKvarService, KvarService>();


// Add services to the container.
builder.Services.AddRazorPages();

var app = builder.Build();

{
    using var scope = app.Services.CreateScope();
    var context = scope.ServiceProvider.GetRequiredService<DapperContext>();
    await context.Init();
}


// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapRazorPages();

app.MapControllers();

app.Run();
