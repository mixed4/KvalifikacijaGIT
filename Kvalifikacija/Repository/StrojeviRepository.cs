﻿using dapper.postgre.api.Context;
using Dapper;
using Kvalifikacija.Dto;
using Kvalifikacija.Models;
using System.Data;

namespace Kvalifikacija.Repository
{
    public interface IStrojeviRepository
    {
        public Task<IEnumerable<Stroj>> GetStrojeve();
        public Task<Stroj> GetStroj(int id);
        public Task<Stroj> GetByNaziv(string naziv);
        public Task<Stroj> CreateStroj(StrojForCreationDto stroj);
        public Task UpdateStroj(int id, StrojForUpdateDto stroj);
        public Task DeleteStroj(int id);
        public Task<Stroj> GetStrojeviKvaroviMultipleResults(int num, int page, string ImeStroja);
        public Task<Stroj> GetStrojeviKvaroviDESC(int num, int page, string ImeStroja);
        public Task<Stroj> GetStrojeviKvaroviASC(int num, int page, string ImeStroja);
        public Task GetProsjekTrajanja(string ImeStroja);


    }
    public class StrojeviRepository : IStrojeviRepository
    {
        private readonly DapperContext _context;
        public StrojeviRepository(DapperContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Stroj>> GetStrojeve()
        {
            using var connection = _context.CreateConnection();
            var sql = "SELECT * FROM strojevi";
            var strojevi = await connection.QueryAsync<Stroj>(sql);
            return strojevi;
        }

        public async Task<Stroj> GetStroj(int id)
        {
            var query = "SELECT * FROM strojevi WHERE idstroja = @id";
            using (var connection = _context.CreateConnection())
            {
                var stroj = await connection.QuerySingleOrDefaultAsync<Stroj>(query, new { id });
                return stroj;
            }
        }
        public async Task<Stroj> GetByNaziv(string imestroja)
        {
            using var connection = _context.CreateConnection();
            var sql = "SELECT * FROM strojevi WHERE imestroja = @imestroja";
            return await connection.QuerySingleOrDefaultAsync<Stroj>(sql, new { imestroja });
        }

        public async Task<Stroj> CreateStroj(StrojForCreationDto stroj)
        {

            var query = "INSERT INTO strojevi (imestroja) VALUES (@imestroja)";
            var parameters = new DynamicParameters();
            parameters.Add("imestroja", stroj.ImeStroja, DbType.String);

            using (var connection = _context.CreateConnection())
            {
                var id = await connection.QuerySingleAsync<int>(query, parameters);
                var createdStroj = new Stroj
                {
                    IdStroja = id,
                    ImeStroja = stroj.ImeStroja
                };
                return createdStroj;
            }
        }

        public async Task UpdateStroj(int id, StrojForUpdateDto stroj)
        {
            var query = "WITH subq AS( SELECT AVG(AGE ( VrijemeZavrsetka, VrijemePocetka )) AS prosjektrajanja FROM kvarovi) " +
                "UPDATE strojevi SET imestroja = @imestroja,prosjektrajanja = subq.prosjektrajanja FROM subq WHERE idstroja = @idstroja";
            var parameters = new DynamicParameters();
            parameters.Add("idstroja", id, DbType.Int32);
            parameters.Add("imestroja", stroj.ImeStroja, DbType.String);
            parameters.Add("ProsjekTrajanja", stroj.ProsjekTrajanja, DbType.String);

            using (var connection = _context.CreateConnection())
            {
                await connection.ExecuteAsync(query, parameters);
            }
        }

        //Delete specific Machine by ID
        public async Task DeleteStroj(int id)
        {
            using var connection = _context.CreateConnection();

            var query = "DELETE FROM strojevi WHERE idstroja = @id";
            
            await connection.ExecuteAsync(query, new { id });
        }

        //GET specific Machine with all of its Breakdowns, without sorting
        public async Task<Stroj> GetStrojeviKvaroviMultipleResults(int num, int page,string ImeStroja)
        {
            var query = "SELECT * FROM Strojevi WHERE ImeStroja = @ImeStroja;" +
                        "SELECT * FROM Kvarovi WHERE ImeStroja = @ImeStroja LIMIT @num OFFSET (10 * (@page - 1)); ";

            using (var connection = _context.CreateConnection())
            using (var multi = await connection.QueryMultipleAsync(query, new { num, page, ImeStroja }))
            {
                var stroj = await multi.ReadSingleOrDefaultAsync<Stroj>();
                if (stroj != null)
                    stroj.KvarList = (await multi.ReadAsync<Kvar>()).ToList();
                return stroj;
            }
        }

        //GET specific Machine with all of its Breakdowns, with sorting ASC
        public async Task<Stroj> GetStrojeviKvaroviASC(int num, int page, string ImeStroja)
        {
            var query = "SELECT * FROM Strojevi WHERE ImeStroja = @ImeStroja;" +
                        "SELECT * FROM Kvarovi WHERE ImeStroja = @ImeStroja ORDER BY prioritet ASC, vrijemepocetka DESC LIMIT @num OFFSET (10 * (@page - 1)); ";

            using (var connection = _context.CreateConnection())
            using (var multi = await connection.QueryMultipleAsync(query, new { num, page, ImeStroja }))
            {

                var strojevi = await multi.ReadSingleOrDefaultAsync<Stroj>();
                if (strojevi != null)
                    strojevi.KvarList = (await multi.ReadAsync<Kvar>()).ToList();
                
                return strojevi;
            }
        }

        //GET specific Machine with all of its Breakdowns, with sorting DESC
        public async Task<Stroj> GetStrojeviKvaroviDESC(int num, int page, string ImeStroja)
        {
            var query = "SELECT * FROM Strojevi WHERE ImeStroja = @ImeStroja;" +
                        "SELECT * FROM Kvarovi WHERE ImeStroja = @ImeStroja ORDER BY prioritet DESC, vrijemepocetka DESC LIMIT @num OFFSET (10 * (@page - 1)); ";
            using (var connection = _context.CreateConnection())
            using (var multi = await connection.QueryMultipleAsync(query, new { num, page, ImeStroja }))
            {
                var strojevi = await multi.ReadSingleOrDefaultAsync<Stroj>();
                
                if (strojevi != null)
                    strojevi.KvarList = (await multi.ReadAsync<Kvar>()).ToList();
                return strojevi;
            }
        }

        //Calculate Average duration of Breakdowns on specific machine
        public async Task GetProsjekTrajanja(string ImeStroja)
        {
            var query = "WITH subq AS( SELECT AVG(AGE ( VrijemeZavrsetka, VrijemePocetka )) AS prosjektrajanja FROM kvarovi) " +
                        "UPDATE strojevi SET prosjektrajanja = subq.prosjektrajanja FROM subq WHERE ImeStroja = @ImeStroja;";
            var connection = _context.CreateConnection();
            await connection.ExecuteAsync(query, new { ImeStroja });

        }
    }
}
