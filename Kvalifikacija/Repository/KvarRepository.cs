﻿using dapper.postgre.api.Context;
using Dapper;
using Kvalifikacija.Dto;
using Kvalifikacija.Models;
using System.Data;

namespace Kvalifikacija.Repository
{
    public interface IKvarRepository
    {
        public Task<IEnumerable<Kvar>> GetKvarove();
        public Task<Kvar> GetKvar(int id);
        public Task<Kvar> GetByNaziv(string naziv);
        public Task<Kvar> CreateKvar(KvarForCreationDto kvar);
        public Task UpdateKvar(int id, KvarForUpdateDto kvar);
        public Task DeleteKvar(int id);
    }
    public class KvarRepository : IKvarRepository
    {
        private readonly DapperContext _context;
        public KvarRepository(DapperContext context)
        {
            _context = context;
        }

        //sql string for selecting all Breakdowns
        public async Task<IEnumerable<Kvar>> GetKvarove()
        {
            using var connection = _context.CreateConnection();
            var sql = "SELECT * FROM kvarovi";
            var kvari = await connection.QueryAsync<Kvar>(sql);
            return kvari;
        }

        //sql string for selecting one specific Breakdown by ID
        public async Task<Kvar> GetKvar(int id)
        {
            var query = "SELECT * FROM kvarovi WHERE IdKvara = @id";
            using (var connection = _context.CreateConnection())
            {
                var kvar = await connection.QuerySingleOrDefaultAsync<Kvar>(query, new { id });
                return kvar;
            }
        }

        //sql string for selecting one specific Breakdown by NAME
        public async Task<Kvar> GetByNaziv(string naziv)
        {
            using var connection = _context.CreateConnection();
            var sql = "SELECT * FROM kvarovi WHERE naziv = @naziv";
            return await connection.QuerySingleOrDefaultAsync<Kvar>(sql, new { naziv });
        }

        //Create a new Breakdown
        public async Task<Kvar> CreateKvar(KvarForCreationDto kvar)
        {
            //Status == True znači da je riješeno, Status == False znači da nije riješeno
            var query = "INSERT INTO kvarovi (Naziv,ImeStroja,Prioritet,VrijemePocetka,VrijemeZavrsetka,Opis,Status)" +
                " VALUES (@Naziv,@ImeStroja,@Prioritet,@VrijemePocetka,@VrijemeZavrsetka,@Opis,@Status)";
            var parameters = new DynamicParameters();
            parameters.Add("Naziv", kvar.Naziv, DbType.String);
            parameters.Add("ImeStroja", kvar.ImeStroja, DbType.String);
            parameters.Add("Prioritet", kvar.Prioritet, DbType.Int32);
            parameters.Add("VrijemePocetka", kvar.VrijemePocetka, DbType.Date);
            parameters.Add("VrijemeZavrsetka", kvar.VrijemeZavrsetka, DbType.Date);
            parameters.Add("Opis", kvar.Opis, DbType.String);
            parameters.Add("Status", kvar.Status, DbType.Boolean);

            var sql = "SELECT * FROM kvarovi WHERE ImeStroja = @ImeStroja";

            //Checks if the new Breakdowns is labeled as finished, if TRUE creates a new breakdown without any issue
            if (kvar.Status==true) { 
            using (var connection = _context.CreateConnection())
            {
                var id = await connection.QuerySingleAsync<int>(query, parameters);
                var createdKvar = new Kvar
                {
                    IdKvara = id,
                    Naziv = kvar.Naziv,
                    ImeStroja = kvar.ImeStroja,
                    Prioritet = kvar.Prioritet,
                    VrijemePocetka=kvar.VrijemePocetka,
                    VrijemeZavrsetka=kvar.VrijemeZavrsetka,
                    Opis = kvar.Opis,
                    Status = kvar.Status
                };
                return createdKvar;
            }
            }
            else
            {
                var connection = _context.CreateConnection();
                var kvarovi = await connection.QueryAsync<Kvar>(sql,parameters);
                //Checks if the new Breakdowns is labeled as finished, 
                //if FALSE checks if there is another Breakdown on the same Machine labeles also as FALSE 

                if(kvarovi != null) {
                    foreach (var item in kvarovi)
                    {
                        //if found throws an error
                        if (item.Status == false)
                        {
                            throw new Exception("Već postoji otvoren Kvar koji nije završen");
                        }
                    }
                    //In case a Machine is NOT found with an existing open/unfinished Breakdown, a new Breakdown is created
                    var id = await connection.QuerySingleAsync<int>(query, parameters);
                    var createdKvar = new Kvar
                    {
                        IdKvara = id,
                        Naziv = kvar.Naziv,
                        ImeStroja = kvar.ImeStroja,
                        Prioritet = kvar.Prioritet,
                        VrijemePocetka = kvar.VrijemePocetka,
                        VrijemeZavrsetka = kvar.VrijemeZavrsetka,
                        Opis = kvar.Opis,
                        Status = kvar.Status
                    };
                    return createdKvar;
                }
                //IF no other Breakdowns are found at all, create a new Breakdown
                else
                {
                    var id = await connection.QuerySingleAsync<int>(query, parameters);
                    var createdKvar = new Kvar
                    {
                        IdKvara = id,
                        Naziv = kvar.Naziv,
                        ImeStroja = kvar.ImeStroja,
                        Prioritet = kvar.Prioritet,
                        VrijemePocetka = kvar.VrijemePocetka,
                        VrijemeZavrsetka = kvar.VrijemeZavrsetka,
                        Opis = kvar.Opis,
                        Status = kvar.Status
                    };
                    return createdKvar;
                }
            }
        }

        //Update existing Breakdown
        public async Task UpdateKvar(int id, KvarForUpdateDto kvar)
        {
            var query = "UPDATE kvarovi SET Naziv = @Naziv, ImeStroja=@ImeStroja, Prioritet=@Prioritet, VrijemePocetka=@VrijemePocetka, " +
                "VrijemeZavrsetka=@VrijemeZavrsetka, Opis=@Opis, Status=@Status WHERE IdKvara = @id";
            var parameters = new DynamicParameters();
            parameters.Add("IdKvara", id, DbType.Int32);
            parameters.Add("Naziv", kvar.Naziv, DbType.String);
            parameters.Add("ImeStroja", kvar.ImeStroja, DbType.String);
            parameters.Add("Prioritet", kvar.Prioritet, DbType.Int32);
            parameters.Add("VrijemePocetka", kvar.VrijemePocetka, DbType.Date);
            parameters.Add("VrijemeZavrsetka", kvar.VrijemeZavrsetka, DbType.Date);
            parameters.Add("Opis", kvar.Opis, DbType.String);
            parameters.Add("Status", kvar.Status, DbType.Boolean);

            var sql = "SELECT * FROM kvarovi WHERE ImeStroja = @ImeStroja";

            //If Status is True update the Brakedown without issue
            if (kvar.Status == true)
            {
                using (var connection = _context.CreateConnection())
                {
                    await connection.ExecuteAsync(query, parameters);
                }
            }
            else
            //If Status is False check if there is another Breakdown labeled FALSE
            {
                var connection = _context.CreateConnection();
                var kvarovi = await connection.QueryAsync<Kvar>(sql);
                foreach (var item in kvarovi)
                {
                    //If found throw error
                    if (item.Status == false)
                    {
                        throw new Exception("Status nemože biti 'True' jer postoji kvar koji još nije riješen");
                    }
                }
                //If no Breakdown is found with Status as False update the Table
                using (var connection = _context.CreateConnection())
                {
                    await connection.ExecuteAsync(query, parameters);
                }
            }
        }

        //Delete specific Breakdown by ID
        public async Task DeleteKvar(int id)
        {
            using var connection = _context.CreateConnection();

            var query = "DELETE FROM kvarovi WHERE IdKvara = @id";

            await connection.ExecuteAsync(query, new { id });
        }

        
    }
}