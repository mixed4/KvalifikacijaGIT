﻿using Kvalifikacija.Dto;
using Kvalifikacija.Services;
using Microsoft.AspNetCore.Mvc;

namespace Kvalifikacija.Controllers
{
    
    [ApiController]
    [Route("stroj")]
    public class StrojController : ControllerBase
    {
        private readonly IStrojService _strojRepo;
        public StrojController(IStrojService strojRepo)
        {
            _strojRepo = strojRepo;
        }
        [HttpGet] //Dohvati sve Strojeve, bez Kvarova (u Postman odabrati GET), Get all Machines, without Breakdowns (GET in Postman)
        public async Task<IActionResult> GetStrojeve()
        {
            try
            {
                var strojevi = await _strojRepo.GetStrojeve();
                return Ok(strojevi);
            }
            catch (Exception ex)
            {
                //log error
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("{id}")] //Dohvati zaseban Stroj (u Postman odabrati GET), Get specific Machine (GET in Postman)
        public async Task<IActionResult> GetStroj(int id)
        {
            try
            {
                var stroj = await _strojRepo.GetStroj(id);
                if (stroj == null)
                    return NotFound();
                return Ok(stroj);
            }
            catch (Exception ex)
            {
                //log error
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost] //Kreiraj novi Stroj (u Postman odabrati POST), Create a new Machine (POST in Postman)
        public async Task<IActionResult> CreateStroj(StrojForCreationDto stroj)
        {
            try
            {
                await _strojRepo.CreateStroj(stroj);
                return Ok(new { message = "Stroj dodan u bazu" });
            }
            catch (Exception ex)
            {
                //log error
                return StatusCode(500, ex.Message);
            }
        }
        [HttpPut("{id}")] //Ažuriraj postojeći stroj (u Postman odabrati PUT), Update existing Machine (PUT in Postman)
        public async Task<IActionResult> UpdateStroj(int id, StrojForUpdateDto stroj)
        {
            try
            {
                await _strojRepo.UpdateStroj(id, stroj);
                return Ok(new { message = "Stroj promjenjen" });
            }
            catch (Exception ex)
            {
                //log error
                return StatusCode(500, ex.Message);
            }
        }

        [HttpDelete("{id}")] //Izbriši stroj ako postoji Npr. https://localhost:7117/stroj/1 (u Postman odabrati DELETE); Delete a specific Machine if it exists example https://localhost:7117/stroj/1 (DELETE in Postman)
        public async Task<IActionResult> DeleteStroj(int id)
        {
            try
            {
                var dbStroj = await _strojRepo.GetStroj(id);
                if (dbStroj == null)
                    return NotFound("Navedeni stroj ne postoji");

                await _strojRepo.DeleteStroj(id);
                return Ok("Stroj izbrisan");
            }
            catch (Exception ex)
            {
                //log error
                return StatusCode(500, ex.Message);
            }
        }


        //Dohvati stroj i sve kvarove s paginacijom, bez sortitranja; GET all machines with their specific Breakdowns, with pagination, without sorting
        //example: https://localhost:7117/stroj/Samsung/MultipleResult/2/1 (GET in Postman)
        [HttpGet("{ImeStroja}/MultipleResult/{num}/{page}")] 
        public async Task<IActionResult> GetStrojeviKvaroviMultipleResults(int num, int page, string ImeStroja)
        {
            try
            {
                //Call GET Machines and Breakdowns
                var stroj = await _strojRepo.GetStrojeviKvaroviMultipleResults(num, page, ImeStroja);
                //Call Average Breakdown duration script
                await _strojRepo.GetProsjekTrajanja(ImeStroja);

                if (stroj == null)
                    return NotFound();

                return Ok(stroj);
            }
            catch (Exception ex)
            {
                //log error
                return StatusCode(500, ex.Message);
            }
        }

        //Dohvati stroj i sve kvarove s paginacijom, s sortitranjem; GET all machines with their specific Breakdowns, with pagination, with sorting ASC
        //example: https://localhost:7117/stroj/Samsung/MultipleResultASC/2/1 (GET in Postman)
        [HttpGet("{ImeStroja}/MultipleResultASC/{num}/{page}")] 
        public async Task<IActionResult> GetStrojeviKvaroviASC(int num, int page, string ImeStroja)
        {
            try
            {
                //Call GET Machines and Breakdowns Sorted ASC
                var stroj = await _strojRepo.GetStrojeviKvaroviASC(num, page, ImeStroja);
                //Call Average Breakdown duration script
                await _strojRepo.GetProsjekTrajanja(ImeStroja);
                if (stroj == null)
                    return NotFound();

                return Ok(stroj);
            }
            catch (Exception ex)
            {
                //log error
                return StatusCode(500, ex.Message);
            }
        }

        //Dohvati stroj i sve kvarove s paginacijom, s sortitranjem; GET all machines with their specific Breakdowns, with pagination, with sorting DESC
        //example: https://localhost:7117/stroj/Samsung/MultipleResultDESC/2/1 (GET in Postman)
        [HttpGet("{ImeStroja}/MultipleResultDESC/{num}/{page}")] 
        public async Task<IActionResult> GetStrojeviKvaroviDESC(int num, int page, string ImeStroja)
        {
            try
            {
                //Call GET Machines and Breakdowns Sorted DESC
                var stroj = await _strojRepo.GetStrojeviKvaroviDESC(num, page, ImeStroja);
                //Call Average Breakdown duration script
                await _strojRepo.GetProsjekTrajanja(ImeStroja);

                if (stroj == null)
                    return NotFound();

                return Ok(stroj);
            }
            catch (Exception ex)
            {
                //log error
                return StatusCode(500, ex.Message);
            }
        }
    }
}
