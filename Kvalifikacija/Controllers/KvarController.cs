﻿using Kvalifikacija.Dto;
using Kvalifikacija.Services;
using Microsoft.AspNetCore.Mvc;

namespace Kvalifikacija.Controllers
{
    [ApiController]
    [Route("kvar")]
    public class KvarController : ControllerBase
    {
        private readonly IKvarService _kvarRepo;
        public KvarController(IKvarService kvarRepo)
        {
            _kvarRepo = kvarRepo;
        }
        [HttpGet] // Dohvati sve kvarove; Get all Breakdowns
        public async Task<IActionResult> GetKvarove()
        {
            try
            {
                var kvarovi = await _kvarRepo.GetKvarove();
                return Ok(kvarovi);
            }
            catch (Exception ex)
            {
                //log error
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("{id}")] //Dohvati određeni kvar; Get Specific Breakdown
        public async Task<IActionResult> GetKvar(int id)
        {
            try
            {
                var kvar = await _kvarRepo.GetKvar(id);
                if (kvar == null)
                    return NotFound();
                return Ok(kvar);
            }
            catch (Exception ex)
            {
                //log error
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost] //Kreiraj novi kvar; Create a new Breakdown
        public async Task<IActionResult> CreateKvar(KvarForCreationDto kvar)
        {
            try
            {
                await _kvarRepo.CreateKvar(kvar);
                return Ok(new { message = "Kvar dodan u bazu" });
            }
            catch (Exception ex)
            {
                //log error
                return StatusCode(500, ex.Message);
            }
        }
        [HttpPut("{id}")] //Ažuriraj postojeći kvar; Update an existing Breakdown
        public async Task<IActionResult> UpdateKvar(int id, KvarForUpdateDto kvar)
        {
            try
            {
                await _kvarRepo.UpdateKvar(id, kvar);
                return Ok(new { message = "Kvar promjenjen" });
            }
            catch (Exception ex)
            {
                //log error
                return StatusCode(500, ex.Message);
            }
        }

        [HttpDelete("{id}")]//Izbriši određeni kvar; Delete specific Breakdown
        public async Task<IActionResult> DeleteKvar(int id)
        {
            try
            {
                var dbKvar = await _kvarRepo.GetKvar(id);
                if (dbKvar == null)
                    return NotFound("Navedeni kvar ne postoji");

                await _kvarRepo.DeleteKvar(id);
                return Ok("Kvar izbrisan");
            }
            catch (Exception ex)
            {
                //log error
                return StatusCode(500, ex.Message);
            }
        }
    }
}
