﻿using Kvalifikacija.Context;
using Kvalifikacija.Dto;
using Kvalifikacija.Models;
using Kvalifikacija.Repository;

namespace Kvalifikacija.Services
{
    public interface IKvarService
    {
        public Task<IEnumerable<Kvar>> GetKvarove();
        public Task<Kvar> GetKvar(int id);
        public Task<Kvar> CreateKvar(KvarForCreationDto kvar);
        public Task UpdateKvar(int id, KvarForUpdateDto kvar);
        public Task DeleteKvar(int id);
    }
    public class KvarService : IKvarService
    {
        private IKvarRepository _kvarClass;


        public KvarService(
            IKvarRepository kvarClass
            )
        {
            _kvarClass = kvarClass;
        }

        public async Task<IEnumerable<Kvar>> GetKvarove()
        {
            return await _kvarClass.GetKvarove();
        }

        public async Task<Kvar> GetKvar(int id)
        {
            var kvar = await _kvarClass.GetKvar(id);

            if (kvar == null)
            {
                throw new KeyNotFoundException("Kvar ne postoji");
            }

            return kvar;
        }

        public async Task<Kvar> CreateKvar(KvarForCreationDto kvar)
        {

            var kvarstat = await _kvarClass.CreateKvar(kvar);
            if (kvarstat == null){
                throw new AppException("Greška kod unosa novog kvara");
            }
            else
            {
                return kvarstat;
            }


        }

        public async Task UpdateKvar(int id, KvarForUpdateDto kvar)
        {
            var var = await _kvarClass.GetKvar(id);

            if (var == null)
                throw new KeyNotFoundException("Kvar ne postoji");

            await _kvarClass.UpdateKvar(id, kvar);
        }

        public async Task DeleteKvar(int id)
        {
            await _kvarClass.DeleteKvar(id);
        }
    }
}
