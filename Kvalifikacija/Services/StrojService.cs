﻿using Kvalifikacija.Context;
using Kvalifikacija.Dto;
using Kvalifikacija.Models;
using Kvalifikacija.Repository;


namespace Kvalifikacija.Services
{
    public interface IStrojService
    {
        public Task<IEnumerable<Stroj>> GetStrojeve();
        public Task<Stroj> GetStroj(int id);
        public Task<Stroj> CreateStroj(StrojForCreationDto stroj);
        public Task UpdateStroj(int id, StrojForUpdateDto stroj);
        public Task DeleteStroj(int id);
        public Task<Stroj> GetStrojeviKvaroviMultipleResults(int num, int page, string ImeStroja);
        public Task<Stroj> GetStrojeviKvaroviDESC(int num, int page, string ImeStroja);
        public Task<Stroj> GetStrojeviKvaroviASC(int num, int page, string ImeStroja);
        public Task GetProsjekTrajanja(string ImeStroja);


    }
    public class StrojService : IStrojService
    {
        private IStrojeviRepository _kvalifikacijaClass;


        public StrojService(
            IStrojeviRepository kvalifikacijaClass
            ) { 
            _kvalifikacijaClass = kvalifikacijaClass;
        }

        public async Task<IEnumerable<Stroj>> GetStrojeve()
        {
            return await _kvalifikacijaClass.GetStrojeve();
        }

        public async Task<Stroj> GetStroj(int id)
        {
            var stroj = await _kvalifikacijaClass.GetStroj(id);

            if(stroj == null)
            {
                throw new KeyNotFoundException("Stroj ne postoji");
            }

            return stroj;
        }

        public async Task<Stroj> CreateStroj(StrojForCreationDto stroj)
        {
            if (await _kvalifikacijaClass.GetByNaziv(stroj.ImeStroja!) != null)
            {
                throw new AppException("Stroj s navedenim nazivom '" + stroj.ImeStroja + "' postoji");
            }


            return await _kvalifikacijaClass.CreateStroj(stroj);
            
            
        }

        public async Task UpdateStroj(int id, StrojForUpdateDto stroj)
        {
            var var = await _kvalifikacijaClass.GetStroj(id);

            if (var == null)
                throw new KeyNotFoundException("Stroj ne postoji");

            await _kvalifikacijaClass.UpdateStroj(id, stroj);
        }

        
        public async Task DeleteStroj(int id)
        {
            await _kvalifikacijaClass.DeleteStroj(id);
        }

        public async Task<Stroj> GetStrojeviKvaroviMultipleResults(int num, int page, string ImeStroja)
        {
            var var = await _kvalifikacijaClass.GetStrojeviKvaroviMultipleResults(num, page, ImeStroja);

            if (var == null)
                throw new KeyNotFoundException("Stroj ne postoji");

            return var;
        }
        public async Task<Stroj> GetStrojeviKvaroviASC(int num, int page, string ImeStroja)
        {
            var var = await _kvalifikacijaClass.GetStrojeviKvaroviASC(num, page, ImeStroja);

            if (var == null)
                throw new KeyNotFoundException("Stroj ne postoji");

            return var;
        }
        public async Task<Stroj> GetStrojeviKvaroviDESC(int num, int page, string ImeStroja)
        {
            var var = await _kvalifikacijaClass.GetStrojeviKvaroviDESC(num, page, ImeStroja);

            if (var == null)
                throw new KeyNotFoundException("Stroj ne postoji");

            return var;
        }

        public async Task GetProsjekTrajanja(string ImeStroja)
        {
            await _kvalifikacijaClass.GetProsjekTrajanja(ImeStroja);
        }
    }
}
