﻿using Dapper;
using Kvalifikacija.Context;
using Microsoft.Extensions.Options;
using Npgsql;
using System.Data;

namespace dapper.postgre.api.Context
{
    public class DapperContext
    {
        private DbSettings _dbSettings;

        public DapperContext(IOptions<DbSettings> dbSettings)
        {
            _dbSettings = dbSettings.Value;
        }

        //Connection strin call
        public IDbConnection CreateConnection()
        {
            var connectionString = $"Host={_dbSettings.Server}; Port={_dbSettings.Port}; Database={_dbSettings.Database}; Username={_dbSettings.UserId}; Password={_dbSettings.Password};";
            return new NpgsqlConnection(connectionString);
        }
        public async Task Init()
        {
            await _initDatabase();
            await _initTables();
        }

        // Ako ne postoji baza napravi novu; IF there is no DATABASE create a new one
        private async Task _initDatabase()
        {
            var connectionString = $"Host={_dbSettings.Server};Port={_dbSettings.Port}; Database=postgres; Username={_dbSettings.UserId}; Password={_dbSettings.Password};";
            using var connection = new NpgsqlConnection(connectionString);
            var sqlDbCount = $"SELECT COUNT(*) FROM pg_database WHERE datname = '{_dbSettings.Database}';";
            var dbCount = await connection.ExecuteScalarAsync<int>(sqlDbCount);
            if (dbCount == 0)
            {
                var sql = $"CREATE DATABASE \"{_dbSettings.Database}\"";
                await connection.ExecuteAsync(sql);
            }
        }

        //Ako ne postoji tablica napravi novu; IF there are no TABLES create new ones
        private async Task _initTables()
        {
            using var connection = CreateConnection();
            await _initTAB();

            async Task _initTAB()
            {
                var sql = "CREATE TABLE IF NOT EXISTS Strojevi (IdStroja SERIAL PRIMARY KEY,ImeStroja VARCHAR, ProsjekTrajanja VARCHAR); CREATE TABLE IF NOT EXISTS Kvarovi (IdKvara SERIAL PRIMARY KEY,Naziv VARCHAR," +
                    "ImeStroja VARCHAR, Prioritet INTEGER, VrijemePocetka DATE, VrijemeZavrsetka DATE, Opis VARCHAR, Status BOOLEAN);";
                await connection.ExecuteAsync(sql);
            }
        }

    }
}