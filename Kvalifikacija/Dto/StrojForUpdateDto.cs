﻿namespace Kvalifikacija.Dto
{   // Update Dto for Machines
    public class StrojForUpdateDto
    {
        public string ImeStroja { get; set; }
        public TimeSpan ProsjekTrajanja { get; set; }

    }
}
