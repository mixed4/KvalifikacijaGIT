﻿namespace Kvalifikacija.Dto
{   // Update Dto for Breakdowns
    public class KvarForUpdateDto
    {
        public string Naziv { get; set; }

        public string ImeStroja { get; set; }

        public int Prioritet { get; set; }

        public DateTime VrijemePocetka { get; set; }

        public DateTime VrijemeZavrsetka { get; set; }

        public string Opis { get; set; }

        public bool Status { get; set; }
    }
}
