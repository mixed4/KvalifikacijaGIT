﻿namespace Kvalifikacija.Dto
{   // Create Dto for Machines
    public class StrojForCreationDto
    {
        public string ImeStroja { get; set; }

        public TimeSpan ProsjekTrajanja { get; set; }

    }
}
